import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent {
  @Input() condition: boolean = false;
  @Input() data: any[] = [];
  @Input() columns: string[] = [];
  @Input() startIndex: number = 0;
  @Input() header: string[] = [];
  @Input() currentSortColumn: string = '';
  @Input() isSortAsc: boolean = true;
  @Output() detailClicked = new EventEmitter<any>();
  @Output() editClicked = new EventEmitter<any>();
  @Output() deleteClicked = new EventEmitter<any>();
  @Output() sortClicked = new EventEmitter<{
    column: string;
    direction: 'asc' | 'desc';
  }>();

  getHeader(): string[] {
    return this.columns.length > 0
      ? this.columns
      : Object.keys(this.data[0] || {});
  }

  getHeaderTable(): string[] {
    return this.header.length > 0 ? this.header : this.columns;
  }

  onDetailClick(item: any) {
    this.detailClicked.emit(item);
  }

  onEditClick(item: any) {
    this.editClicked.emit(item);
  }

  onDeleteClick(item: any) {
    this.deleteClicked.emit(item);
  }

  onSortClick(column: string, direction: 'asc' | 'desc') {
    this.currentSortColumn = column;
    this.isSortAsc = direction === 'asc';
    this.sortClicked.emit({ column, direction });
  }

  isLastColumn(index: number): boolean {
    return index === this.columns.length - 0;
  }
  isPictureColumn(column: string): boolean {
    return column === 'Picture';
  }
}
