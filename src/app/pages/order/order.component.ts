import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {
  debounceTime,
  distinctUntilChanged,
  map,
  switchMap,
} from 'rxjs/operators';
import { DataService } from '../../services/data.service';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrl: './order.component.css',
})
export class OrderComponent implements OnInit {
  orders: any[] = [];
  customers: any[] = [];
  products: any[] = [];
  pagination: any = {};
  orderHeaderTable: string[] = [
    'No',
    'Customer Name',
    'Product Name',
    'Order Data',
    'Quantity',
    'Total Price',
    'Action',
  ];
  orderColumns: string[] = [
    'No',
    'customerName',
    'itemName',
    'formatToDateTime',
    'quantity',
    'formattedTotalPrice',
  ];
  pageSize: number = 5;
  startIndex: number = 0;
  condition: boolean = true;
  showModalDetail: boolean = false;
  showModalEdit: boolean = false;
  showModalDelete: boolean = false;
  showModalAdd: boolean = false;
  orderDetails: any;
  errorMessageOrder: string = '';
  searchForm!: FormGroup;
  searchResults: any[] = [];
  selectedProduct: any = null;
  sortedData: any[] = [];
  currentSortColumn: string = '';
  isSortAsc: boolean = true;

  quantityControl: FormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(1),
    Validators.maxLength(10),
    Validators.pattern(/^[1-9][0-9]*$/),
  ]);

  addOrderForm = new FormGroup({
    customerId: new FormControl('', [Validators.required]),
    itemId: new FormControl('', [Validators.required]),
    quantity: this.quantityControl,
    totalPrice: new FormControl('', [Validators.required]),
  });

  editOrderForm = new FormGroup({
    customerId: new FormControl('', [Validators.required]),
    itemId: new FormControl('', [Validators.required]),
    quantity: this.quantityControl,
    totalPrice: new FormControl('', [Validators.required]),
  });

  constructor(private dataService: DataService) {
    this.searchForm = new FormGroup({
      search: new FormControl('', Validators.required),
    });
  }

  ngOnInit(): void {
    this.loadData(1);
    this.searchForm
      .get('search')
      ?.valueChanges.pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap((value) =>
          this.dataService.getOrders(1, this.pageSize, value)
        )
      )

      .subscribe((result) => {
        this.orders = result.data.map((order: any) => {
          return {
            ...order,
            formattedTotalPrice: this.formatToRupiah(order.totalPrice),
            formatToDateTime: this.formatToDateTime(order.orderDate),
          };
        });
        this.pagination = result.pagination;
      });

    this.loadDataCustomer();
    this.loadDataProduct();
    this.quantityControl.valueChanges.subscribe(() => {
      this.updatePrice();
    });
  }

  loadData(page: number, sortOrder: string = ''): void {
    const startIndex = (page - 1) * this.pageSize;
    const searchValue = this.searchForm.get('search')?.value || '';
    this.dataService
      .getOrders(page, this.pageSize, searchValue, sortOrder)
      .subscribe((response: any) => {
        this.orders = response.data.map((order: any) => {
          return {
            ...order,
            formattedTotalPrice: this.formatToRupiah(order.totalPrice),
            formatToDateTime: this.formatToDateTime(order.orderDate),
          };
        });
        this.pagination = response.pagination;
        this.startIndex = startIndex;
      });
  }

  loadDataCustomer(): void {
    this.dataService.getCustomers(1, 50).subscribe((response: any) => {
      this.customers = response.data.map((customer: any) => {
        return {
          customerId: customer.customerId,
          name: customer.customerName,
          phoneNumber: customer.phoneNumber,
        };
      });
    });
  }

  loadDataProduct(): void {
    this.dataService.getItems(1, 50).subscribe((response: any) => {
      this.products = response.data
        .filter((product: any) => product.isAvailable)
        .map((product: any) => {
          return {
            itemId: product.itemId,
            name: product.itemName,
            price: product.itemPrice,
          };
        });
    });
  }

  updatePrice(): void {
    const selectedItemIdAdd = Number(this.addOrderForm.get('itemId')?.value);
    const selectedItemIdEdit = Number(this.editOrderForm.get('itemId')?.value);
    const quantityValueAdd = +this.addOrderForm.get('quantity')?.value || 0;
    const quantityValueEdit = +this.editOrderForm.get('quantity')?.value || 0;

    if (selectedItemIdAdd) {
      this.selectedProduct = this.products.find(
        (product) => product.itemId === selectedItemIdAdd
      );
      const totalPrice = (this.selectedProduct?.price || 0) * quantityValueAdd;
      this.addOrderForm.get('totalPrice')?.setValue(totalPrice.toString());
    } else if (selectedItemIdEdit) {
      this.selectedProduct = this.products.find(
        (product) => product.itemId === selectedItemIdEdit
      );
      const totalPrice = (this.selectedProduct?.price || 0) * quantityValueEdit;
      this.editOrderForm.get('totalPrice')?.setValue(totalPrice.toString());
    }
  }

  onSubmitAdd(): void {
    if (this.addOrderForm.valid) {
      const formData = this.addOrderForm.value;

      this.dataService.addOrder(formData).subscribe(
        (response) => {
          alert('Order has been added:\n' + response.orderId);
          window.location.reload();
        },
        (error) => {
          alert('Order Failed to Add');
          console.error('Error adding order:', error);
        }
      );
    } else {
      this.addOrderForm.markAllAsTouched();
    }
  }

  onSubmitEdit(): void {
    if (this.editOrderForm.valid) {
      const formData = this.editOrderForm.value;
      const orderId = this.orderDetails.orderId;

      this.dataService.editOrder(orderId, formData).subscribe(
        (response) => {
          alert('Order has been updated:\n' + response.orderId);
          this.loadData(1);
          this.showModalEdit = false;
          window.location.reload();
        },
        (error) => {
          console.error('Error updating order', error);
        }
      );
    } else {
      this.addOrderForm.markAllAsTouched();
    }
  }

  onSubmitDelete(): void {
    if (this.orderDetails) {
      this.dataService.deleteOrder(this.orderDetails).subscribe(
        () => {
          alert('Order has been deleted:\n' + this.orderDetails.orderId);
          this.loadData(1);
          this.showModalDelete = false;
        },
        (error) => {
          console.error('Error deleting order', error);
        }
      );
    }
  }

  onDownload(): void {
    this.dataService.downloadOrder().subscribe(
      (response) => {
        alert('Orders downloaded successfully');
      },
      (error) => {
        alert('Failed to download orders');
        console.error('Error downloading orders', error);
      }
    );
  }

  onSort({ column, direction }: { column: string; direction: 'asc' | 'desc' }) {
    this.currentSortColumn = column;
    this.isSortAsc = direction === 'asc';

    const joinSort =
      column + direction.charAt(0).toUpperCase() + direction.slice(1);
    const sortVariable = joinSort
      .replace(/([a-z])([A-Z])/g, '$1_$2')
      .toUpperCase();

    this.loadData(1, sortVariable);
  }

  formatToRupiah(amount: number): string {
    const formatter = new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR',
      minimumFractionDigits: 2,
    });
    return formatter.format(amount).replace('IDR', 'Rp.');
  }

  formatToDateTime(dateTime: string): string {
    const date = new Date(dateTime);
    return date.toLocaleString('id-ID', { timeZone: 'UTC' });
  }

  onPageChanged(page: number): void {
    let sortOrder = '';
    if (this.currentSortColumn) {
      const direction = this.isSortAsc ? 'asc' : 'desc';
      const joinSort = this.currentSortColumn + '_' + direction.toUpperCase();
      sortOrder = joinSort.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase();
    }
    this.loadData(page, sortOrder);
  }

  onPageSizeChanged(pageSize: number): void {
    let sortOrder = '';
    if (this.currentSortColumn) {
      const direction = this.isSortAsc ? 'asc' : 'desc';
      const joinSort = this.currentSortColumn + '_' + direction.toUpperCase();
      sortOrder = joinSort.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase();
    }
    this.pageSize = pageSize;
    this.loadData(1, sortOrder);
  }

  hideModal(): void {
    this.showModalDetail = false;
    this.showModalEdit = false;
    this.showModalDelete = false;
    this.showModalAdd = false;
  }

  resetForm() {
    this.addOrderForm.reset();
    this.editOrderForm.reset();
  }

  onAdd() {
    this.showModalAdd = true;
  }

  onDetail(order: any): void {
    this.showModalDetail = true;
    this.orderDetails = order;
  }

  onEdit(order: any): void {
    this.showModalEdit = true;
    this.orderDetails = order;
  }

  onDelete(order: any) {
    this.showModalDelete = true;
    this.orderDetails = order;
  }
  get customerId() {
    return this.addOrderForm.get('customerId');
  }

  get itemId() {
    return this.addOrderForm.get('itemId');
  }

  get quantity() {
    return this.addOrderForm.get('quantity');
  }
}
