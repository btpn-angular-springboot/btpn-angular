/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts}"],
  theme: {
    extend: {
      screens: {
        mobileS: "320px",
        mobileM: "375px",
        mobileL: "425px",
        tablet: "640px",
        laptop: "1024px",
        desktop: "1280px",
      },
      colors: {
        customGrey: "#949c9e",
        customDarkGrey: "#e6f3f7",
        customBrokenWhite: "#e8ebec",
        customBlue: "#03d5ff",
        customRed: "#f00",
      },
    },
  },
  plugins: [],
};
